require('dotenv').config()
const express = require('express')

const app = express()
const port = process.env.PORT

const { getWeatherByCityName } = require('./services/weather.service')

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/weather/:city', getWeatherByCityName);

const server = app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = { app, server };