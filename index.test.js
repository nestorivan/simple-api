const { app, server } = require("./index");
const supertest = require('supertest');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');

const api = supertest(app)

const {apiBasePath} = require('./services/weather.service')

const fakeData = {
  "coord": {
      "lon": -103.3333,
      "lat": 20.6667
  },
  "weather": [
      {
          "id": 804,
          "main": "Clouds",
          "description": "overcast clouds",
          "icon": "04d"
      }
  ],
  "base": "stations",
  "main": {
      "temp": 295.48,
      "feels_like": 295.7,
      "temp_min": 295.05,
      "temp_max": 296.02,
      "pressure": 1021,
      "humidity": 74
  },
  "visibility": 6437,
  "wind": {
      "speed": 1.54,
      "deg": 170
  },
  "clouds": {
      "all": 90
  },
  "dt": 1630694646,
  "sys": {
      "type": 2,
      "id": 2032170,
      "country": "MX",
      "sunrise": 1630672681,
      "sunset": 1630717654
  },
  "timezone": -18000,
  "id": 4005539,
  "name": "Guadalajara",
  "cod": 200
}

describe('Sample Test', () => {
  it('should display Hello World!', async () => {
    const res = await api.get("/");

    expect(res.status).toBe(200);
    expect(res.text).toEqual("Hello World!");
  });

  it('weather api call return OK status', async() => {
    var mock = new MockAdapter(axios);
    mock.onGet(apiBasePath, {
      params: {
        q: "Guadalajara"
      }
    }).reply(200, fakeData)

    const res = await axios.get(apiBasePath, {
      params: {
        q: "Guadalajara"
      }
    });

    expect(res.status).toBe(200);
    expect(res.data).toEqual(fakeData);
  });

  it('weather api call return Network Error status', async() => {
    var mock = new MockAdapter(axios);
    mock.onGet(apiBasePath, {
      params: {
        q: "Guadalajara"
      }
    }).networkError()

    try{
    const res = await axios.get(apiBasePath, {
      params: {
        q: "Guadalajara"
      }
    });
    }catch(err){

      expect(err.message).toBe("Network Error");
    }

  });
})

afterAll(() => {
  server.close()
})