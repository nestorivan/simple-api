# Simple API

Simple API project

## Stack
* Node
* Express
* Jest
* Axios


## Installation

To download dependencies run:
```npm install```

To run project
```npm start```

To run tests
```npm run jest```

To run tests in watch mode
```npm run jest:watch```


## License
[MIT](https://choosealicense.com/licenses/mit/)