const axios = require('axios');

const basePath = `http://api.openweathermap.org/data/2.5/weather`

const getWeatherByCityName = async (req,res,next) => {
  try {
    const { data:result } = await axios.get(basePath, {
      params:{
        q: req.params.city,
        appid: process.env.API_KEY
      }
    })

    return res.send(result)

  } catch(err){
    next(err)
  }
}


module.exports = { getWeatherByCityName, apiBasePath: basePath }